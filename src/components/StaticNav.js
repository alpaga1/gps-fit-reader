/**
 * 
 */

import React from 'react';

/* Node modules */
import {Nav} from 'react-bootstrap';
import PropTypes from 'prop-types';

/**
 * Build static nav from array
 * @param {Object} param0 Params component
 * @return {Object} React node
 */
export function StaticNav({tabs}) {
  return (
    <Nav>
      {tabs.map((tab, index) => (
        <Nav.Item key={index}>
          <Nav.Link onSelect={() => console.log(tab.key)}>{tab.key}</Nav.Link>
        </Nav.Item>
      ))}
    </Nav>
  )
}

StaticNav.propTypes = {
  tabs: PropTypes.array.isRequired,
};
