/**
 * 
 */

/* Node modules */
import styled from 'styled-components';
import {Container} from 'react-bootstrap';

export const Grid = styled(Container)`
  display: flex;
  jutify-content: center;

  padding: 5%;
  height: 100%;

  background-color: #121212;
`;
