/**
 *
 */

import React from 'react';

/* Scenes */
import {Home} from './scenes';

import 'bootstrap/dist/css/bootstrap.min.css';

/**
 * App GPS fit Reader
 * @return {Object} React node
 */
function App() {
  return <Home />
}

export default App;
