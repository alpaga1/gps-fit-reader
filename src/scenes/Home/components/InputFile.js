/**
 * 
 */

import React from 'react';

/* Node modules */
import {Form, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
 
/* Utils */
import {fileReader} from '../../../utils/fileReader.js';
import {readFitFile} from '../../../utils/easyFit.js';

 /**
  * Build all disign for fit file
  * @param {Object} param0 Params compoments
  * onRead: Callback when user wants to read file
  * @return {Object} React node
  */
export function InputFile({onRead}) {
  /* File selected in input */
  const [currentFile, updateCurrentFile] = React.useState(null);

  const _handleSubmit = async (value) => {
    value.preventDefault();
    /* Check if file was selected */
    if (!currentFile) return;
    
    /* Read content file */
    try {
      const content = await fileReader(currentFile);
      const informations = await readFitFile(content);
      onRead(false, informations);
    } catch (error) {
      onRead(true, error);
    }
  }

  const _handleChangeFile = (e) => {
    updateCurrentFile(e.target.files[0]);
  }

  return (
    <Form onSubmit={_handleSubmit} className='w-50'>
      <Form.Group controlId='currentFile'>
        <Form.Control type='file' required onChange={_handleChangeFile} size='lg' />
      </Form.Group>
      <Button variant="outline-success" className="mt-2" type="submit">Analyser votre fichier</Button>
    </Form>
   )
 }
 
 InputFile.propTypes = {
  onRead: PropTypes.func.isRequired,
};
 