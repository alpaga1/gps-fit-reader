/**
 * 
 */

import React from 'react';

/* Node modules */
import PropTypes from 'prop-types';
import {Container, Row, Col, Card, ListGroup} from 'react-bootstrap';
import styled from 'styled-components';
import humanizeDuration from 'humanize-duration';

/* Global components */

function CardSession({title, infos}) {
  const Title = styled.p`
    font-weight: bold;
  `;

  return (
    <Col>
    <Card border='success'>
      <Card.Header><h4>{title}</h4></Card.Header>
      <Card.Body>
        <ListGroup variant="flush">
          {
            infos.map((info, index) => (
              <ListGroup.Item key={index}>
                <Row>
                <Col md='auto'><Title>{info.name}</Title></Col>
                <Col>{info.value}{info.type}</Col>
                </Row>
              </ListGroup.Item>
            ))
          }
      </ListGroup>
      </Card.Body>
    </Card>
    </Col>
  )
}

CardSession.propTypes = {
  title: PropTypes.string.isRequired,
  infos: PropTypes.array.isRequired,
  type: PropTypes.string.isRequired,
};

function Session({session}) {
  console.log(session);
  return (
    <Row xs={1} md={2} className="g-4">
      <CardSession title='Altitude' 
        infos={[
          {name: 'Max', value: session.max_altitude.toFixed(2), type: ' km'},
          {name: 'Moy', value: session.avg_altitude.toFixed(2), type: ' km'},
          {name: 'Min', value: session.min_altitude.toFixed(2), type: ' km'},
        ]}
      />

      <CardSession title='Temperature' 
        infos={[
          {name: 'Max', value: session.max_temperature, type: '°'},
          {name: 'Moy', value: session.avg_temperature, type: '°'},
        ]}
      />

      <CardSession title='Vitesse' 
        infos={[
          {name: 'Max', value: session.max_speed.toFixed(2), type: ' km/h'},
          {name: 'Moy', value: session.avg_speed.toFixed(2), type: ' km/h'},
        ]}
      />

      <CardSession title='Puissance' 
        infos={[
          {name: 'Max', value: session.max_power},
          {name: 'Moy', value: session.avg_power},
        ]}
      />

      {/* <CardSession title='Heures' 
        infos={[
          {name: 'Départ', value: session.start_time},
          {name: 'Arrivée', value: session.timestamp},
        ]}
      /> */}

      <CardSession title='Total' 
        infos={[
          {name: 'Ascent', value: session.total_ascent.toFixed(2), type: ' km'},
          {name: 'Descent', value: session.total_descent.toFixed(2), type: ' km'},
          {name: 'Calories', value: session.total_calories, type: ' Kcal'},
          {name: 'Distance', value: session.total_distance.toFixed(2), type: ' km'},
          {name: 'Temps', value: humanizeDuration(session.total_elapsed_time * 1000, {round: true, units: ["h", "m"]})},
          {name: 'Temps en mouvement', value: humanizeDuration(session.total_moving_time * 1000, {round: true, units: ["h", "m"]})},
        ]}
      />

    </Row>
  )
}

Session.propTypes = {
  session: PropTypes.object.isRequired
}

/**
 * Build all disign for fit file
 * @param {Object} param0 Params compoments
 * data: All informations in device
 * @return {Object} React node
 */
export function Device({data}) {
  return (
    <Container>
      <Row>
      <Col>
        <h4 className='text-success'>Session</h4>
        <Session session={data.activity.sessions[0]} />
      </Col>
      <Col>
        <h4 className='text-success'>Laps</h4>
      </Col>
      </Row>
    </Container>
  )
}

Device.propTypes = {
  data: PropTypes.object.isRequired,
};
