/**
 * 
 */

import React from 'react';

/* Node modules */
import {Row, Col} from 'react-bootstrap';

/* Global components */
import {Grid} from '../../components/layout.js';
import {Logo} from '../../components/img.js';

/* Local components */
import {InputFile} from './components/InputFile.js';
import {Device} from './components/Device.js';

/* Assets */
import FitGPS  from '../../assets/img/logo.png';

/**
 * Scene Home
 * @return {Object} React node
 */
export function Home() {
  /* Data in current file */
  const [data, updateData] = React.useState(null);

  /* Handle user click on read file */
  const _handleOnRead = (error, infos) => {
    if (error) {
      console.error(infos);
      return;
    }
    updateData(infos)
  }

  return (
    <Grid fluid>
      <Col>
      <Row className='mb-2'><Logo src={FitGPS}/></Row>
      {/* File to use */}
      <Row className='d-flex justify-content-end align-items-center mb-5'>
        <InputFile onRead={_handleOnRead} />
      </Row>
      {/* Body */}
      <Row>
        {!data ? null : <Device data={data} />}
      </Row>
      </Col>
    </Grid>
  );
}
