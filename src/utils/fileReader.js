/**
 * 
 */

/* Node modules */
import PropTypes from 'prop-types';

/**
 * Read a local file
 * @param {String} file File to read
 * @return {String} Content file
 */
export function fileReader(file) {
  return new Promise((resolve) => {
    const reader = new FileReader();
      reader.onload = () => {
        resolve(reader.result);
      };
    reader.readAsArrayBuffer(file);
  });
}

fileReader.Proptypes = {
  file: PropTypes.string.isRequired,
};
