/**
 * 
 */

/* Node modules */
import PropTypes from 'prop-types';
import EasyFit from 'easy-fit';

/* Default value to read the file */
const defaultOptions = {
  force: true,
  speedUnit: 'km/h',
  lengthUnit: 'km',
  temperatureUnit: 'celsius',
  elapsedRecordField: true,
  mode: 'cascade',
};

/**
 * Read and parse fit file
 * @param {String} file Filepath
 * @param {Object} options Option for easy fit module
 * @return {Object} Informations in file
 */
export function readFitFile(file, options = defaultOptions) {
  return new Promise((resolve, reject) => {
    console.log(options)
    const easyFit = new EasyFit(options);
    easyFit.parse(file, function(error, data) {
      if (error) {
        reject(error);
      } else {
        resolve(data);
      }
    });
  });
}

readFitFile.prototypes = {
  file: PropTypes.string.isRequired,
  options: PropTypes.shape({
    mode: PropTypes.string,
    lengthUnit: PropTypes.string,
    temperatureUnit: PropTypes.string,
    speedUnit: PropTypes.string,
    force: PropTypes.bool,
    elapsedRecordField: PropTypes.bool,
  }),
};
